var test = require('tape');

var Transaction = require('../lib/transaction')
var TransactionLog = require('../lib/transaction_log');

test('Basic Transaction Log', function t1(assert) {
	var tests = [
		{
			t: new Transaction(1, "A"),
			a: {
				1: "A"
			}
		},
		{
			t: new Transaction(3, "C"),
			a: {
				1: "A",
				3: "C"
			}
		},
		{
			t: new Transaction(2, "B"),
			a: {
				1: "A",
				2: "B",
				3: "C"
			}
		}
	];
	function handler(data, t) {
		data[t.id] = t.data;
		return data;
	}
	function reverter(data, t) {
		delete data[t.id];
		return data;
	}
	var tl = new TransactionLog({}, 500, handler, reverter);

	tests.forEach(function _fet(test) {
		tl.handleNewTransaction(test.t);
		assert.deepEqual(tl.data, test.a, "Data checks out");
	});

	assert.end();
});