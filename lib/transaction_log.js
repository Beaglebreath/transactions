
module.exports = TransactionLog;

function TransactionLog(initData, maxQueueLength, handler, reverter) {
	this.queue = [];
	this.maxQueueLength = maxQueueLength;
	this.data = initData;
	this.handler = handler;
	this.reverter = reverter;
}

TransactionLog.prototype.handleNewTransaction =
	function handleNewTransaction(transaction) {
	var transactionsToRevert = [];
	var found = false;
	for (var i = this.queue.length - 1; i >= 0; --i) {
		var t = this.queue[i];
		if (transaction.id > t.id) {
			console.log(transaction + '>' + t);
			// It should go in front
			this.queue.splice(i, 0, transaction);
			found = true;
			break;
		}
		transactionsToRevert.push(t)
	}
	if (!found && this.queue.length > 0) {
		// TODO: Error out some how
		return
	}
	// For each transaction that actually occurred after, revert them
	for (var r = transactionsToRevert.length - 1; r >= 0; --r) {
		this._revert(transactionsToRevert[r]);
	}
	// Then handle the transaction properly
	this._handle(transaction);
	// Then redo everything that was reverted
	for (var h = 0; h < transactionsToRevert.length; ++h) {
		this._handle(transactionsToRevert[h]);
	}
};

TransactionLog.prototype._handle = function _handle(t) {
	this.data = this.handler(this.data, t);
};

TransactionLog.prototype._revert = function _revert(t) {
	this.data = this.reverter(this.data, t);
};